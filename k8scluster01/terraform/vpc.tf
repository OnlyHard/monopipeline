data "aws_vpc" "vpc_0" {
  tags = {
    env_name_group = var.env_name
    function = "vpc"
  }
}

data "aws_subnet" "subnet_0" {
  tags = {
    env_name_group = var.env_name
    function = "generalsubnet"
  }
}