# Get latest Ubuntu image
data "aws_ami" "ami_0" {
  # https://ubuntu.com/server/docs/cloud-images/amazon-ec2
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_instance" "instance_0" {
  ami                    = data.aws_ami.ami_0.id
  instance_type          = "t3.xlarge"
  subnet_id              = data.aws_subnet.subnet_0.id
  vpc_security_group_ids = [data.aws_security_group.sg_0.id]
  key_name               = aws_key_pair.keypair_0.id
  root_block_device {
    delete_on_termination = true
    volume_size           = "20"
  }
  tags = {
    awx_inventory_group = "k8s_cluster01"
    env_name_group      = "${var.env_name}"
  }
}

resource "aws_key_pair" "keypair_0" {
  key_name_prefix = "${var.env_name}-"
  public_key      = var.env_publickey
}