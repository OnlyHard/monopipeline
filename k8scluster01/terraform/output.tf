output "public_ip_address" {
  description = "The actual ip address allocated for the resource."
  value       = aws_instance.instance_0.public_ip
}